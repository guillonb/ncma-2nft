\makeatletter
\def\input@path{ {./} {sty/} {fig/} {tikz/} {cls/} {tex/} }
\makeatother
\documentclass[italian,UKenglish]{beamer}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{babel}

\usepackage[shortcuts]{extdash}
\usepackage{amsmath,amsthm,amssymb,mathtools}

\usepackage[inline]{enumitem}
\setitemize{label=\usebeamerfont*{itemize item}\usebeamercolor[fg]{itemize item}\usebeamertemplate{itemize item}}
\setenumerate[1]{label=\protect\usebeamerfont*{enumerate item}\protect\usebeamercolor[fg]{enumerate item}\insertenumlabel.}

\usepackage[mode=image|tex]{standalone}

\usepackage{tikz}
\usetikzlibrary{decorations.pathreplacing}

% local packages
\usepackage{mybeamer}
\usepackage{tikzemph}
\usepackage{leftright}
\usepackage{macros}
\usepackage{globalstyles}
\usepackage{algebraic}
\usepackage{logics}

\setcounter{tocdepth}{2}

% title
\title{%
	On nondeterministic two-way transducers%
}
\author{%
	\underline{Bruno Guillon}%
}
\date{%
	{%
		\small%
		\textsc{ncma}%
	}%
	\newline%
	{%
		\scriptsize
		August 21, 2018%
	}%
}
\institute{%
	{\selectlanguage{italian}{Dipartimento di Informatica, Università degli Studi di Milano}}%
}


\begin{document}
\addtocounter{framenumber}{-1}
\begin{frame}
	\widebox[2]{%
		\maketitle
	}%
	\vfill
	\widebox[.5]{%
		\small%
		\textrm{mainly, joint work with Christian Choffrut}%
	}
	\vspace{-4ex}
\end{frame}

\section{Introduction}
\begin{frame}{Outline}
	\tableofcontents
\end{frame}
\subsection{Word transductions}
\begin{frame}{Relations in computer science}
	\emph{Relation $\equiv$ set of tuples}
	\bigskip

	Omnipresent in computer science
	\begin{itemize}
		\item Graph structures
		\item Data bases
		\item Semantics of programs
		\item Rewriting systems
		\item \ldots
	\end{itemize}
	\bigskip

	\pause
	\emph{Transduction $\equiv$ a binary relation}

	\hfill in which an \inC{input} and an \outC{output} are implicitly understood
\end{frame}
\begin{frame}{Word transductions}
	\widebox{\textbf{\large\quad This talk:}}
	\begin{itemize}[itemsep=5mm]
		\item binary relations on words
			\hfill
			$R\subseteq\inC{\Sigma^*}\times\outC{\Delta^*}$
			\hfill\vbox{}
		\item<1-> computed by some kind of \temph[1]{transducers}
			\smallskip

			\hfill
			\uncover<5->{constant-memory \alert<6->{nondeterministic} devices}
	\end{itemize}
	\bigskip
	
	\widebox<2->{%
		\textbf{\large\quad Equivalent formalisms:}
		\begin{itemize}[itemsep=5mm]
			\item<2-> A function from words into languages:
				\hfill
				$
				f_R:\
				\begin{array}{rcl}
					\inC{\Sigma^*}&\rightarrow&\outC{2^{\Delta^*}}\\
					\inC u&\mapsto&\set{\outC v\mid (\inC u,\outC v)\in R}
				\end{array}
				$
			\item<3-> A formal power series:
				\hfill	
				$
				\sigma=
				\sum\limits_{\inC u\in\inC{\Sigma^*}}\outC{\left\langle\sigma,u\right\rangle}\inC u
				$
				\qquad
				with $\outC{\left\langle\sigma,u\right\rangle}=f_R(\inC u)$
			\item<4->
				computed by some kind of \temph[1]{weighted automata} over $\outC{\rat(\oalphab^*)}$

				\hfill
				\nit[cite,font=\small]{Lombardy's talk at NCMA'15 in Porto}%
		\end{itemize}
	}
\end{frame}
\begin{frame}
	\vfill\vfill%
	\vfill\vfill%
	\widebox[1]{%
		\Huge%
		\hfill%
		Which issues arise
		\hfill\hfill\hfill\hfill\vbox{}

		\hfill\hfill\hfill\hfill
		from nondeterminism?%
		\hfill\vbox{}
	}
	\vfill
	\widebox<2->{%
		\large
		\centering
		How can we handle them%
		\uncover<3->{, in some special cases?}
		\vfill\vbox{}
	}%
\end{frame}
\subsection{Automata and transducers}
\begin{frame}{Automata with outputs: 1-way transducers}
	\begin{overlayarea}{\textwidth}{.4\textheight}
		\begin{columns}
			\begin{column}{.3\textwidth}
				\only<1-3>{\includegraphics[page=1,width=\textwidth]{1ft-def}}%
				\only<4->{\includegraphics[page=2,width=\textwidth]{1ft-def}}%
			\end{column}
			\hfill
			\begin{column}{.6\textwidth}
				\begin{inlineexample}<2-3,5->
					\hfill
					\only<2-3>{\includegraphics[page=3,width=\textwidth]{1ft-def}}%
					\only<5->{\includegraphics[page=4,width=\textwidth]{1ft-def}}%
				\end{inlineexample}
			\end{column}
		\end{columns}
	\end{overlayarea}
	\begin{overlayarea}{\textwidth}{.5\textheight}
		\foreach \time [count=\page from 5] in {1-2,3,4-5,6,7,8,9,10}{%
			\only<\time>{\includegraphics[page=\page,width=\textwidth]{1ft-def}}%
		}
	\end{overlayarea}
\end{frame}

\begin{frame}{Examples}
	with~$\inC\ialphab=\outC\oalphab$ fixed
	\bigskip
	\widebox{%
		\begin{itemize}[itemsep=2.5mm]
			\item $\textsc{Identity}:\inC u\mapsto\outC u$
			\item $\textsc{Erase}:\inC u\mapsto\outC\emptyword$
			\item $\textsc{L-Rotate}:\inC{\sigma u}\mapsto\outC{u\sigma}$, for each~$\sigma\in\inC\ialphab=\outC\oalphab$
			\item $\textsc{R-Rotate}:\inC{u\sigma}\mapsto\outC{\sigma u}$, for each~$\sigma\in\inC\ialphab=\outC\oalphab$
			\item $\textsc{Subword}:\set{(\inC u,\outC v)}[\outC v\text{ is a not-necessarily connected subword of~}\inC u]$
		\end{itemize}
	}%
\end{frame}
\begin{frame}{Nondeterminism versus determinism}
	\begin{itemize}[itemsep=4mm]
		\item<1-> functions $\subset$ relations
			
			\hfill
			\eg, no deterministic transducer realize subword
		\item<2-> sequential functions $\subset$ rational functions

			\hfill
			\eg, no deterministic transducer realize right-rotate
		\item<3-> \nit[cite]{Griffith'68}
			\hfill
			equivalence, inclusion, intersection emptiness\ldots

			\hfill
			are undecidable problems
			for nondeterministic transducers
	\end{itemize}
\end{frame}
\begin{frame}{Two-wayness}
	A transducer is defined by:
	\begin{itemize}
		\item an automaton with transition set~$\delta$
		\item a production function from~$\delta$ to~$\outC{\oalphab^*}$
	\end{itemize}
	\bigskip

	\begin{overlayarea}{\textwidth}{.5\paperheight}
		\widebox<2->[1]{%
			{\textbf{\large\quad Two-way transducers:}}
			\vspace{-2ex}
			\begin{columns}[b]
				\begin{column}{.3\paperwidth}
					\includegraphics[width=\textwidth,page=1]{2ft-def}%
				\end{column}
				\begin{column}{.675\paperwidth}
					\only<2>{\includegraphics[width=\textwidth,page=2]{2ft-def}}%
					\only<3>{\includegraphics[width=\textwidth,page=3]{2ft-def}}%
				\end{column}
			\end{columns}
		}%
	\end{overlayarea}
\end{frame}
\begin{frame}{Examples}
	Two\=/wayness extends expressiveness of transducers…

	\begin{itemize}
		\item $\textsc{Square}:\inC u\mapsto\outC{uu}$
		\item $\textsc{Mirror}:\inC u\mapsto\outC{\mirror u}$
			\hfill
			{(\small$\mirror u$ denotes the reverse of~$u$)}
		\item $\textsc{Sort}:\inC u\mapsto\outC{a^{\inC{\length u_a}}b^{\inC{\length u_b}}\cdots z^{\inC{\length u_z}}}$
		\item $\textsc{Powers}:\set{(\inC u,\outC{u^k})}[k\in\mathbb N]$
	\end{itemize}
\end{frame}
\begin{frame}{``Regular'' transductions}
	\widebox{\centering\large The class of \textbf{functions} realized by two-way transducers is \textbf{robust}}
	\bigskip

	\begin{itemize}
		\item closure under composition
			\hfill\nit[cite]{Chytil\&Jákl'77, Dartois\etal'17}
		\item decidable equivalence
			\hfill\nit[cite]{Gurari'80}
		\item alternative characterizations:
			\begin{itemize}
				\item reversible = deterministic = functional
					\hspace{-4em}
					\hfill
					\nit[cite,text width=10.25em,align=right]{%
						Dartois\etal'17,\linebreak
						Engelfriet\&Hoogeboom'01%
					}
				\item \mso word transductions
					\hfill\nit[cite]{Engelfriet\&Hoogeboom'01}
				\item copyless register automata
					\hfill\nit[cite]{Alur\&Černy'10}
				\item ``regular combinators''
					\hfill
					\nit[cite,text width=13.5em,align=right]{%
						Alur\etal'14,\linebreak
						Baudru\&Reynier'18,
						Dave\etal'18%
					}
			\end{itemize}
	\end{itemize}
\end{frame}
\begin{frame}{Expressiveness of transducers}%
	\begin{tikzpicture}[overlay,remember picture]%
		\path
			(current page.south) node[anchor=south,inner sep=0]	{%
				\foreach \time [count=\page] in {1,...,8}{%
					\only<\time>{\includegraphics[page=\page,width=\paperwidth]{ft-map}}%
				}%
			}%
			;
	\end{tikzpicture}%
\end{frame}
\section{Algebraic descriptions of transduction classes}
\label{sec:algebra}
\begin{frame}
	\Huge
	\vfill
	\vfill
	\vfill
	\vfill
		What are the transductions

		\hfill
		realized by \nft[2]?
	\vfill
	\vfill
	\hfill
	\uncover<2->{\normalsize \colorbox{gborange}{\ref{sec:algebra}.}~\secname}
\end{frame}
\subsection{Rational operations}
\begin{frame}{Rational operations}
	\begin{overlayarea}{\textwidth}{.25\textheight}
		\rightwidebox[.75]{%
			\begin{itemize}
				\item \mbox{\alert<3>{set union}}
					\hfill
					$\relC[1]{R_1}\cup\relC[2]{R_2}$
				\item \alert<4>{componentwise concatenation}

					\hfill
					$%
					\relC[1]{R_1}\cdot\relC[2]{R_2}%
					=
					\set{%
						(\relC[1]{u_1}\relC[2]{u_2},\relC[1]{v_1}\relC[2]{v_2})%
						}[%
						\relC[1]{(u_1,v_1)\in R_1}
						\text{ and }
						\relC[2]{(u_2,v_2)\in R_2}
					]
					$
					\medbreak
				\item \alert<5>{Kleene star}
					\hfill
					$%
					\relC[1]{R}^*=
					\set{%
						(\relC[1]{u_1\cdots u_k},\relC[1]{v_1\cdots v_k})
						}[%
						\forall i,\, \relC[1]{(u_i,v_i)\in R}
					]
					$
			\end{itemize}
		}
	\end{overlayarea}
	\begin{overlayarea}{\textwidth}{.7\textheight}
		\widebox[1]{%
			\centering%
			\foreach \time [count=\page]	in {2,...,5}{%
				\only<\time>{\includegraphics[page=\page,width=.9\paperwidth]{fig/1nft_cu_rop}}%
			}%
			\only<2-5>{%

				\begin{overlayarea}{\textwidth}{3ex}
						\only<3>{%
							\begin{minipage}{.4\textwidth}
								\begin{itemize}[leftmargin=0pt]
									\item simulate \relC[1]{$T_1$} or \relC[2]{$T_2$}
								\end{itemize}
							\end{minipage}
						}%
						\only<4>{%
							\begin{minipage}{12.5em}
								\begin{itemize}[leftmargin=0pt,nosep]
									\item simulate \relC[1]{$T_1$} on some prefix
									\item simulate \relC[2]{$T_2$} on corresp. suffix
								\end{itemize}
							\end{minipage}
							\hfill
							\begin{minipage}{\widthof{\eg, $\textsc{Prefix}\!=\!\Cprod{\textsc{Identity}}{\textsc{Erase}}$}}
								\eg, $\textsc{Prefix}\!=\!\Cprod{\textsc{Identity}}{\textsc{Erase}}$
							\end{minipage}
						}%
						\only<5>{%
							\begin{minipage}{10em}
								\begin{itemize}[leftmargin=0pt]
									\item repeat

										simulate \relC[1]{$T_1$}
										or accept
								\end{itemize}
							\end{minipage}
							\hfill
							\begin{minipage}{\widthof{\eg, $\textsc{Subword}=\Kstar{(\textsc{Identity}\cup\textsc{Erase})}$}}
								\eg, $\textsc{Subword}=\Kstar{(\textsc{Identity}\cup\textsc{Erase})}$
							\end{minipage}
						}%
				\end{overlayarea}
			}%
		}%
		\widebox[.75]{%
			\only<6->{%
				\begin{inlinedefinition}<6->[\rat]
					The class of \emph{rational relations}
					is the smallest class
					\medskip

					\begin{itemize}[nosep]
						\item including finite relations
						\item closed under rational operations
					\end{itemize}
				\end{inlinedefinition}
				\bigskip
				\begin{minipage}[b]{.48\textwidth}%
					\begin{inlinetheorem}<7->\nit[cite,font=\small]{Elgot \& Mezei'65}
						\medskip

						\hfill
						\nft[1]
						$=$
						rational
						\hfill\vbox{}
					\end{inlinetheorem}
				\end{minipage}
			}
		}
		\begin{tikzpicture}[overlay,remember picture,visible on={<7>}]
			\path (current page.south east)	++(180:6mm)	++(90:4mm)
				node[above left] {\hfill\includegraphics[page=1,scale=.95]{tables}}%
				;
		\end{tikzpicture}
	\end{overlayarea}
\end{frame}

\begin{frame}{Which operations capture behaviors of \nft[2]\s?}
	\centering
	\only<1>{\includegraphics[scale=1,page=2]{tables}}%
	\only<2>{\includegraphics[scale=1,page=3]{tables}}%
\end{frame}

\subsection{Hadamard operations}
\begin{frame}{Abilities of two-way transducers}
	\centering%
	\widebox{%
		\begin{tikzpicture}
			\foreach \x [count=\xi] in {1,...,4,6}{
				\node[visible on={<\xi>}]{\includegraphics[scale=.65,page=\x]{ex-square-old}};
			}
			\bigskip%

			\foreach \x [count=\xi from 7] in {1,...,6,8}{
				\node[visible on={<\xi>}]{\includegraphics[scale=.65,page=\x]{fig/ex-powers-old}};
			}

			\begin{scope}
				[yshift=-3cm,visible on={<1-5>}]
				\node[text width=.6\textwidth]	{%
						\begin{itemize}
							\item<2-> \textcolor{brown}{copy the input word}
							\item<3-> rewind the input tape
							\item<4-> \textcolor{red}{append a copy of the input word}
						\end{itemize}
					};
			\end{scope}
			\begin{scope}
				[yshift=-3cm,visible on={<7-13>}]
				\coordinate[fill=black,circle] (start);
				\uncover<8->{
					\node[above left of=start,left] (copyloop) {copy the input word};
					\draw[->,thick] (start) -- (copyloop);
				}
				\uncover<9->{
					\node[above right of=start,right] (rewind) {rewind the input tape};
					\draw[->,thick] (copyloop) -- (rewind);
				}
				\uncover<10->{
					\draw[->,thick] (rewind) -- (start);
				}
				\uncover<13->{
					\node[below of=start] (final) {accept and halt with nondeterminism};
					\draw[->,thick] (start) -- (final);
				}
			\end{scope}
			\begin{scope}
				[visible on={<14->}]
				\node[font=\Large] {\textsc{Square}, \textsc{Powers} $\notin\rat$};
			\end{scope}
		\end{tikzpicture}
	}%
\end{frame}
\begin{frame}{Hadamard operations}
	\begin{overlayarea}{\textwidth}{.25\textheight}
		\rightwidebox[.75]{%
			\begin{itemize}
				\item \mbox{\alert<3>{set union}}
					\hfill
					$\relC[1]{R_1}\cup\relC[2]{R_2}$
				\item \alert<4>{Hadamard product}

					\hfill
					$%
					\Hprod{\relC[1]{R_1}}{\relC[2]{R_2}}%
					=
					\set{%
						(u,\relC[1]{v_1}\relC[2]{v_2})%
						}[%
						\relC[1]{(u,v_1)\in R_1}
						\text{ and }
						\relC[2]{(u,v_2)\in R_2}
					]
					$
					\medbreak
				\item \alert<5>{Hadamard star}
					\hfill
					$%
					\Hstar{\relC[1]{R}}=
					\set{%
						(u,\relC[1]{v_1\cdots v_k})
						}[%
						\forall i,\, \relC[1]{(u,v_i)\in R}
					]
					$
			\end{itemize}
		}
	\end{overlayarea}
	\begin{overlayarea}{\textwidth}{.6\textheight}
		\widebox{%
			\centering%
			\foreach \time [count=\page]	in {2,...,5}{%
				\only<\time>{\vspace{6ex}\includegraphics[page=\page,width=.9\paperwidth]{fig/2nft_cu_Hop}}%
			}%
			\only<2-5>{%
				\vspace{-4ex}

				\begin{overlayarea}{\textwidth}{3ex}%
					\begin{columns}
						\only<3>{%
							\begin{column}{.5\textwidth}
								\begin{itemize}[leftmargin=0pt]
									\item simulate \relC[1]{$T_1$} or \relC[2]{$T_2$}
								\end{itemize}%
							\end{column}
							\hfill
							\begin{column}{.5\textwidth}
							\end{column}
						}%
						\only<4>{%
							\begin{column}{.4\textwidth}
								\begin{itemize}[leftmargin=0pt]
									\item simulate \relC[1]{$T_1$}
									\item rewind the \inC{input} tape
									\item simulate \relC[2]{$T_2$}
								\end{itemize}%
							\end{column}
							\hfill
							\begin{column}{.6\textwidth}
								\eg, $\textsc{Square}=\Hprod{\textsc{Identity}}{\textsc{Identity}}$
							\end{column}
						}%
						\only<5>{%
							\begin{column}{.5\textwidth}
								\begin{itemize}[leftmargin=0pt,nosep]
									\item repeat
										\begin{itemize}[nosep]
											\item simulate \relC[1]{$T_1$}
											\item rewind the \inC{input} tape
										\end{itemize}
									\item or accept nondeterministically
								\end{itemize}%
							\end{column}
							\hfill
							\begin{column}{.5\textwidth}
								\eg, $\textsc{Powers}=\Hstar{\textsc{Identity}}$
							\end{column}
						}%
					\end{columns}
				\end{overlayarea}
			}
		}%
		\only<6->{%
			\begin{overlayarea}{\textwidth}{.2\textheight}
				\widebox[.75]{%
					\vspace{-3ex}
					\begin{inlinedefinition}<6->[\had]
						The class of \emph{Hadamard relations}
						is the smallest class
						\medskip

						\begin{itemize}[nosep]
							\item including rational relations
							\item closed under Hadamard operations
						\end{itemize}
					\end{inlinedefinition}
				}
			\end{overlayarea}
			\begin{overlayarea}{\textwidth}{.25\textheight}
				\foreach \time [count=\page from 4] in {7,8,9,{10,12-}}{%
					\only<\time>{\includegraphics[page=\page,width=\textwidth]{tables}}%
				}%
				\only<11>{%
					\vspace{-1ex}
					\hfill\textbf{\Large rotating}\hfill\vbox{}

					\hfill%
					\includegraphics[page=6,height=.25\textwidth]{two-wayness}%
					\hfill\vbox{}%
				}%
			\end{overlayarea}
		}%
	\end{overlayarea}%
\end{frame}
\subsection{Mirror operation}
\begin{frame}{Mirror}
	\centering\Large
	\includegraphics[page=2,width=.75\textwidth]{ex-mirror}
	\bigskip

	\pause
	$\textsc{Mirror}\notin\had$
\end{frame}
\begin{frame}{Mirror operations}
	\begin{overlayarea}{\textwidth}{.3\textheight}
		\rightwidebox[.75]{%
			\begin{itemize}
				\item<6-> set union
					\hfill
					{\footnotesize$\relC[1]{R_1}\cup\relC[2]{R_2}$}
				\item<6-> Hadamard product
					\hfill
					{\footnotesize$%
						\Hprod{\relC[1]{R_1}}{\relC[2]{R_2}}%
						=
						\set{%
							(u,\relC[1]{v_1}\relC[2]{v_2})%
							}[%
							\relC[1]{(u,v_1)\in R_1}
							\text{ and }
							\relC[2]{(u,v_2)\in R_2}
						]
					$}
				\item<6-> Hadamard star
					\hfill
					{\footnotesize$%
						\Hstar{\relC[1]{R}}=
						\set{%
							(u,\relC[1]{v_1\cdots v_k})
							}[%
							\forall i,\, \relC[1]{(u,v_i)\in R}
						]%
					$}%
					\bigskip
				\item\alert<2-4>{mirror}
					\hfill
					$%
					\mirror{\relC[1]R}=
					\set{(\inC{\mirror u},\outC v)}[{%
							\relC[1]{(u,v)\in R}
					}]
					$
			\end{itemize}
		}
	\end{overlayarea}
	\begin{overlayarea}{\textwidth}{.6\textheight}
		\widebox{%
			\centering%
			\foreach \time [count=\page]	in {2,...,4}{%
				\only<\time>{\vspace{6ex}\includegraphics[page=\page,width=.9\paperwidth]{fig/2nft_cu_mirr}}%
			}%
		}
		\only<5->{%
			\begin{overlayarea}{\textwidth}{.2\textheight}
				\widebox[.75]{%
					\vspace{-3ex}
		\begin{inlinedefinition}<5->[\mhad]
			class of \emph{mirror-Hadamard} relations: smallest class
			\medskip

			\begin{itemize}[nosep]
				\item including rational relations
				\item closed under Hadamard operations and mirror
			\end{itemize}
		\end{inlinedefinition}
				}
			\end{overlayarea}
			\begin{overlayarea}{\textwidth}{.25\textheight}
				\widebox[1]{%
					\foreach \time [count=\page from 8] in {7,8,9,{10,12-}}{%
						\only<\time>{\includegraphics[page=\page,width=\paperwidth]{tables}}%
					}%
				}%
				\only<11>{%
					\vspace{-4ex}
					\hfill\textbf{\Large sweeping}\hfill\vbox{}

					\hfill%
					\includegraphics[page=7,height=.25\textwidth]{two-wayness}%
					\hfill\vbox{}%
				}%
			\end{overlayarea}
		}
	\end{overlayarea}
\end{frame}

\section{Unary cases}
\label{sec:unary}
\begin{frame}
	\vfill
	\vfill
	\vfill
	\vfill
	\vfill
	\vfill
	\widebox[1]{%
		\includegraphics[page=12,width=\paperwidth]{tables}%
	}%
	\Huge
	\vfill
	\hfill
	\uncover<2->{Study of particular cases}
	\hfill\vbox{}
	\vfill
	\vfill
	\vfill
	\vfill
	\vfill
	\hfill
	\uncover<3->{\normalsize\colorbox{gborange}{\ref{sec:unary}.}~\secname}
	\vfill
	\uncover<4->{%
		\hfill
		\hfill
		\hfill
		\hfill
		\hfill
		$\card{\inC\ialphab}=1$
		\hfill
		and/or
		\hfill
		$\card{\outC\oalphab}=1$
	}
\end{frame}
\subsection{Commutative outputs}
\begin{frame}{Contribution of mirror}
	\vspace{-3ex}
	\begin{inlineproposition}
		If~$\card{\inC{\ialphab}}=1$
		or~$\card{\outC{\oalphab}}=1$
		then $\had=\mhad$
	\end{inlineproposition}
	\begin{overlayarea}{\textwidth}{4.5cm}%
		\only<2-4>{\widebox[1]{\includegraphics[page=13,width=\paperwidth]{tables}}}%
		\only<5->{\widebox[1]{\includegraphics[page=14,width=\paperwidth]{tables}}}%
	\end{overlayarea}%
	\begin{inlinetheorem}<3->
		\nit[font=\small,text=citecolor,anchor=text]{from}\nit[cite]{Anselmo'90}

		When~$\card{\outC{\oalphab}=1}$,
		\temph{loop-free}[lf] \nft[2] realize rational transductions
	\end{inlinetheorem}
	\begin{tikzpicture}[overlay,remember picture,visible on={<4->}]
		\path
			(lf.south east)
			node[below right,xshift=3em,text width=13em,fill=temphcolor1,shape=rectangle,rounded corners] (lf cases)
			{%
				\vspace{-1.25ex}
				\begin{itemize}[nosep,itemindent=0pt,labelsep=4pt,labelwidth=1em,leftmargin=1em]
					\item deterministic/unambiguous
					\item functional
				\end{itemize}
			}
			;
			\draw[->,very thick,draw=temphcolor1]	(lf.south)	|- (lf cases.west);
	\end{tikzpicture}
\end{frame}
\begin{frame}{From~2\=/way to 1\=/way automata}
	\begin{overlayarea}{\textwidth}{.6\textheight}%
		\widebox{%
			\centering%
			\only<1>{\includegraphics[width=\paperwidth]{2way-to-1way}}%
			\foreach \time [count=\page] in {2,...,16}{%
				\only<\time>{\includegraphics[width=\paperwidth,page=\page]{2way-to-1way}}%
			}%
		}
	\end{overlayarea}%
	\bigskip
	\bigskip

	\begin{overlayarea}{\textwidth}{.1\textheight}%
		\widebox[.5]{%
			\begin{enumerate}
				\item<2-> every accepted word admits a loop-free accepting computation
				\item<4-> the successor relation of crossing sequences is locally testable
			\end{enumerate}
		}
	\end{overlayarea}%
\end{frame}
\subsection{Both alphabets unary}
\begin{frame}{Both unary case}
	\tikz[overlay,remember picture]\path (current page.north)	node[below,font=\Large]{$\inC{\ialphab}=\outC{\oalphab}=\set a$};
	\vspace{-3ex}
	\widebox[.8]{%
		\begin{inlineexamples}
			\begin{itemize}
				\item $
					\textsc{uIdentity}=
					\set{(\inC{a^n},\outC{a^n})}[n\in\mathbb N]
					$
					\hfill$\in\rat$
				\item $
					\textsc{uSquare}=
					\set{(\inC{a^n},\outC{a^{2n}})}[n\in\mathbb N]
					=\Hprod{\textsc{uIdentity}}{\textsc{uIdentity}}
					$
					\hfill$\in\rat$
				\item $
					\textsc{uPowers}=
					\set{(\inC{a^n},\outC{a^{kn}})}[k,n\in\mathbb N]
					=\Hstar{\textsc{uIdentity}}
					$
					\hfill$\notin\rat$
			\end{itemize}
		\end{inlineexamples}
		\bigskip
		\begin{inlinetheorem}<2->\nit[cite]{Choffrut, G.'14}
			\hfill
			When $\card{\inC{\ialphab}}=1$ and $\card{\outC{\oalphab}}=1$,
			\nft[2]=\had
		\end{inlinetheorem}
	}
	\widebox<3->{%
		\includegraphics[page=15,width=\paperwidth]{tables}%
	}%
\end{frame}
\begin{frame}{Proof sketch}
	\widebox[.8]{
		\begin{inlinetheorem}<1->\nit[cite]{Choffrut, G.'14}
			\hfill
			When $\card{\inC{\ialphab}}=1$ and $\card{\outC{\oalphab}}=1$,
			$\nft[2]=\had$
		\end{inlinetheorem}
		\begin{inlineprooof}<2->
			fix a unary transducer~$T$ realizing~$R$
			\begin{itemize}[itemsep=4mm]
				\item<3-> decompose computations in \emph{hits}

					\hfill\emph{hit $\equiv$ computation path between 2 successive visits of endmarkers}

				\item<4-> each family of hits define a relation~$R_{(q,s),(q',s')}$ 
					\smallskip

					\uncover<5->{%
						\small
						\hfill\eg, $(p,\lend)$-to-$(q,\rend)$, $(p,\rend)$-to-$(q,\rend)$
					}
				\item<7-> \alert<10>{%
						\uncover<9->{simulate each hit family with a one-way transducer}
					}

					\hfill
					\uncover<9->{$\implies$}
					\uncover<7->{each $R_{(q,s),(q',s')}\in\rat$}
					\begin{itemize}
						\item<11-> commutative outputs
						\item<11-> \alert<11>{deal with \emph{central loops}}
					\end{itemize}
				\item<6-> $R$ is a combination of~$R_{(q,s),(q',s')}$'s using Hadamard operations

					\uncover<8->{\hfill$\implies\in\had$~\qedsymbol}
			\end{itemize}
		\end{inlineprooof}
	}
\end{frame}
\begin{frame}{Unary central loops}
	\vspace{-2ex}
	\foreach \time [count=\page] in {1,2,3-7,8,9,10,11-}{%
		\only<\time>{\includegraphics[page=\page,width=\textwidth]{center_loop}}%
	}%
	\widebox{%
		\begin{itemize}[itemsep=3mm]
			\item<4-> each language~$\outC L_q\subseteq\Kstar a$ of \outC{outputs} of all central loops around state~$q$
				\smallskip

				\hfill
				\hfill
				satisfies $\Kstar{\outC L_q}=\outC L_q$
				\uncover<5->{$\implies\outC L_q\in\rat$}
				\uncover<6->{$\implies\outC L$ is finitely generated}
				\hfill\vbox{}
			\item<7-> $\exists N\!\in\!\mathbb N$ such that a window of size~$N$ is sufficient to generate each~$\outC L_q$
				\medskip
			\item<12-> \alert<12>{\textbf{Open problem:} bound~$N$?}
		\end{itemize}
	}
\end{frame}
\subsection{Only one alphabet unary}
\begin{frame}{Relax assumptions?}
	\widebox[1]{%
		\includegraphics[page=15,width=\paperwidth]{tables}%
	}%
	\begin{inlinetheorem}<2->
		\nit[cite]{G.'16}
		\hfill
		When~$\card{\inC{\ialphab}}>1$, $\had\subset\nft[2]$%

		\hfill
		When~$\card{\outC{\oalphab}}>1$, $\had\subset\nft[2]$%
	\end{inlinetheorem}
\end{frame}
\begin{frame}{Input-unary witness}
	\begin{center}
		$\textsc{RLPrefix}=\set{(\inC{a^n},\outC{a^mb^m})}[n,m\in\mathbb N,\, m\leq n]$
	\end{center}
	\foreach \time [count=\page] in {1,...,3,4-}{%
		\only<\time>{\includegraphics[page=\page,width=\textwidth]{2pref}}%
	}%
	\bigskip
	\begin{center}
		\uncover<5->{$\textsc{RLPrefix}\notin\had$}
	\end{center}
	\bigskip
	\begin{inlineremark}<6->
		\had is not closed under componentwise concatenation
	\end{inlineremark}
\end{frame}
\begin{frame}{Output-unary witness}
	\begin{center}
		\widebox[1]{%
			\hfill
			$\textsc{Mult1Block}=
			\set{(\inC u,\outC{a^{kn}})}[%
				\inC{u\!\in\!\set{a,\sharp}^*}\!,\,
				k,n\!\in\!\mathbb N,
				\text{ and }\inC{\sharp a^n\sharp}\text{ is a factor of }\inC u%
			]$
			\hfill\vbox{}
		}
	\end{center}
	\foreach \time [count=\page] in {1,...,7,8-}{%
		\only<\time>{\includegraphics[page=\page,width=\textwidth]{counter_example}}%
	}%
	\bigskip
	\begin{center}
		\uncover<9->{$\textsc{Mult1Block}\notin\had$}
	\end{center}
	\bigskip
	\begin{inlineremark}<9->
		\had is not closed under componentwise concatenation
	\end{inlineremark}
\end{frame}
\begin{frame}{Two-way versus componentwise concatenation}
	\begin{inlineproposition}
		\quad\nft[2] is not closed under

		\hfill
		componentwise concatenation
		\hfill
		and Kleene star
	\end{inlineproposition}
	\bigskip

	\begin{inlineproposition}<2->
		\quad
		\nft[2] is closed under
		
		\hfill
		\textbf{unambiguous} componentwise concatenation%

		\hfill
		and \textbf{unambiguous} Kleene star%
	\end{inlineproposition}
	\begin{inlineremark}<3->
		these operations are used in the characterization of regular functions through regular combinators

		\hfill
		\nit[cite]{%
			Alur\etal'14,
			Baudru\&Reynier'18,
			Dave\etal'18%
		}
	\end{inlineremark}
	\bigskip

	\uncover<4->{but they are still not sufficient for the non-functional case}
\end{frame}
\section*{Conclusion}
\begin{frame}{Conclusion}
	\widebox[1]{%
		\includegraphics[page=15,width=\paperwidth]{tables}%
	}%
	
	\pause
	Abilities arising from nondeterminism:
	\begin{itemize}
		\item<+-> loop
			\begin{itemize}
				\item output an unbounded word in one step
					\hfill
					\eg, $\textsc{Erase}^{-1}$
				\item loop over some portion of the input word
					\hfill
					\eg, \textsc{Powers}
			\end{itemize}
		\item<+-> nondeterministically select some positions
			\hfill
			\eg, \textsc{RLPrefix}
	\end{itemize}
	\bigskip

	\pause
	\vfill
	\hfill
	\temph{Thank you}
\end{frame}	
\end{document}
